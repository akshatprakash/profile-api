This is a profile microservice built using spring boot

It uses gradle to build

Comes with a Dockerfile for scalable deployment

This API must be used in the following sequence

1. User authenticates to OIDC provider on Web
2. User has access to a web app which now allows him to create, update profile
3. Create calls the microservice profile with a PUT method and json profile data
4. Update calls the microservice profile with a POST method and json profile data with the profile id to update
5. Delete can only be called if the logged in user is an admin




                            +--------------+
                            |     WEB      |
                            |              |
                            +---+----------+
                                |     |           +--------------------+
                                |     +---------->|                    |
                                |                 |                    |
                     +----------v-----+           |   TLS/SSL w OIDC   |
                     |                +----------->                    |
                     |      API       <-----------+---------^----------+
                     |                |                     |
                     |                |                     |
                     +--------^-------+                     |
                              |                             |
                              |                             |
                              |                             |
                              |                             |
                              |        +----------+         |
                              |        |          |         |
                              +------->|   CRM    +---------+
                                       |          |
                                       +----------+
                                       
                                       
You can use mvn or gradle to compile the project
You can take this container to a docker swarm/kubernetes/aws yourself or run the Makefile to start serving

There are unit tests which run on maven test
There are BDD tests which can be extended using the profile.feature file

