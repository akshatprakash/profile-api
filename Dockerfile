FROM java:openjdk-8

ADD build/libs/profile-1.jar /app

ENTRYPOINT exec java -jar /profile-1.jar