package com.qantas.profile.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Address
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-10-31T21:11:17.446Z")

public class Address   {
  @JsonProperty("email")
  private String email = null;

  @JsonProperty("home")
  private String home = null;

  @JsonProperty("work")
  private String work = null;

  public Address email(String email) {
    this.email = email;
    return this;
  }

   /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(example = "Qantas", required = true, value = "")
  @NotNull


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Address home(String home) {
    this.home = home;
    return this;
  }

   /**
   * Get home
   * @return home
  **/
  @ApiModelProperty(example = "123 Short St. Sydney NSW 2000", value = "")


  public String getHome() {
    return home;
  }

  public void setHome(String home) {
    this.home = home;
  }

  public Address work(String work) {
    this.work = work;
    return this;
  }

   /**
   * Get work
   * @return work
  **/
  @ApiModelProperty(example = "203 Coward St, Mascot NSW 2020", value = "")


  public String getWork() {
    return work;
  }

  public void setWork(String work) {
    this.work = work;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Address address = (Address) o;
    return Objects.equals(this.email, address.email) &&
        Objects.equals(this.home, address.home) &&
        Objects.equals(this.work, address.work);
  }

  @Override
  public int hashCode() {
    return Objects.hash(email, home, work);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Address {\n");
    
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    home: ").append(toIndentedString(home)).append("\n");
    sb.append("    work: ").append(toIndentedString(work)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

