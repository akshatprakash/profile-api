package com.qantas.profile.api;

import java.util.UUID;
import com.qantas.profile.model.UserProfile;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import javax.validation.constraints.*;
import javax.validation.Valid;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-10-31T21:11:17.446Z")

@Controller
public class ProfileApiController implements ProfileApi {



    public ResponseEntity<Void> createProfile(@ApiParam(value = "User profile to create" ,required=true ) @RequestBody UserProfile userProfile) {
        // do some magic!
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Void> deleteProfile(@ApiParam(value = "User profile id to delete",required=true ) @PathVariable("id") UUID id) {
        // do some magic!
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Void> updateProfile(@ApiParam(value = "User profile id to update",required=true ) @PathVariable("id") UUID id) {
        // do some magic!
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
