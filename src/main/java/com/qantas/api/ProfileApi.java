package com.qantas.api;

import com.qantas.model.UserProfile;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-10-18T11:42:54.455Z")

@Api(value = "profile", description = "the profile API")
public interface ProfileApi {

    @ApiOperation(value = "creates a new profile for the organization", notes = "Adds an profile to the organization", response = Void.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "profile created", response = Void.class),
        @ApiResponse(code = 400, message = "invalid input, object invalid", response = Void.class),
        @ApiResponse(code = 409, message = "an existing profile already exists", response = Void.class) })
    @RequestMapping(value = "/profile",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.PUT)
    ResponseEntity<Void> createProfile(@ApiParam(value = "User profile to create"  ) @RequestBody UserProfile userProfile);


    @ApiOperation(value = "delets a profile", notes = "Deletes a profile from the organization", response = Void.class, tags={ "admins", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "profile deleted", response = Void.class),
        @ApiResponse(code = 400, message = "invalid input, object invalid", response = Void.class),
        @ApiResponse(code = 404, message = "user profile not found", response = Void.class) })
    @RequestMapping(value = "/profile",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.DELETE)
    ResponseEntity<Void> deleteProfile(@ApiParam(value = "User profile to delete"  ) @RequestBody UserProfile userProfile);


    @ApiOperation(value = "updates a user profile", notes = "Adds a profile to the organization", response = Void.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "profile updated", response = Void.class),
        @ApiResponse(code = 400, message = "invalid input, object invalid", response = Void.class),
        @ApiResponse(code = 404, message = "profile not found", response = Void.class) })
    @RequestMapping(value = "/profile",
        produces = { "application/json" }, 
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<Void> updateProfile(@ApiParam(value = "User profile to update"  ) @RequestBody UserProfile userProfile);

}
