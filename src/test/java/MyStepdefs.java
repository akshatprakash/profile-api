import com.qantas.profile.model.UserProfile;
import cucumber.api.PendingException;
import cucumber.api.java8.En;

import java.util.List;

public class MyStepdefs implements En {
    public MyStepdefs() {
        Given("^users:$", (List<UserProfile> userProfiles) -> {
            // Write code here that turns the phrase above into concrete actions
            System.out.format("UserProfiles: ", userProfiles);
        });
        When("^i create users$", () -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
        Then("^i check if create user http status is (\\d+)$", (Integer arg0) -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
        And("^i check if a valid uuid was sent in the response$", () -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
        And("^i check if create user response contains:$", () -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
        Then("^i update user:$", () -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
        Then("^i check if update user http status is (\\d+)$", (Integer arg0) -> {
            System.out.format("Http Status: ", arg0);
        });
        And("^i store id from user response$", () -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
        Then("^i delete user using id$", () -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
        Then("^i check if delete user http status is (\\d+)$", (Integer arg0) -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
        And("^i check if no data was sent in the response$", () -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
    }
}
