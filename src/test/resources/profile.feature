Feature: create, update delete user

  Scenario: i create user
    Given users:
      | id                                   | firstName | lastName | dateOfBirth | address.email    | address.work                  | organization |
      | e53ef4cc-3e03-48e0-8088-fa95847f87bf | Jane      | Doe      | 25-10-1974  | jane@example.com | 123 Short St. Sydney NSW 2000 | snug         |
    When i create users
    Then i check if create user http status is 201
    And i check if a valid uuid was sent in the response
    And i check if create user response contains:
      | id                                   | firstName | lastName | dateOfBirth | address.email    | address.work                  | organization |
      | e53ef4cc-3e03-48e0-8088-fa95847f87bf | Jane      | Doe      | 25-10-1974  | jane@example.com | 123 Short St. Sydney NSW 2000 | snug         |

  Scenario: i update user
    Given users:
      | id                                   | firstName | lastName | dateOfBirth | address.email    | address.work                  | organization |
      | e53ef4cc-3e03-48e0-8088-fa95847f87bf | Jane      | Doe      | 25-10-1974  | jane@example.com | 123 Short St. Sydney NSW 2000 | snug         |
    When i create users
    Then i check if create user http status is 201
    And i check if a valid uuid was sent in the response
    And i check if create user response contains:
      | id                                   | firstName | lastName | dateOfBirth | address.email    | address.work                  | organization |
      | e53ef4cc-3e03-48e0-8088-fa95847f87bf | Jane      | Doe      | 25-10-1974  | jane@example.com | 123 Short St. Sydney NSW 2000 | snug         |
    Then i update user:
      | id                                   | firstName | lastName | dateOfBirth | address.email    | address.work                  | organization |
      | e53ef4cc-3e03-48e0-8088-fa95847f87bf | Jane      | Doe      | 25-10-1974  | jane@example.com | 123 Short St. Sydney NSW 2000 | snug         |
    Then i check if update user http status is 201

  Scenario: i delete user
    Given users:
      | id                                   | firstName | lastName | dateOfBirth | address.email    | address.work                  | organization |
      | e53ef4cc-3e03-48e0-8088-fa95847f87bf | Jane      | Doe      | 25-10-1974  | jane@example.com | 123 Short St. Sydney NSW 2000 | snug         |
    When i create users
    Then i check if create user http status is 201
    And i check if a valid uuid was sent in the response
    And i check if create user response contains:
      | id                                   | firstName | lastName | dateOfBirth | address.email    | address.work                  | organization |
      | e53ef4cc-3e03-48e0-8088-fa95847f87bf | Jane      | Doe      | 25-10-1974  | jane@example.com | 123 Short St. Sydney NSW 2000 | snug         |
    And i store id from user response
    Then i delete user using id
    Then i check if delete user http status is 201

  Scenario: i create user with invalid email
    Given users:
      | id                                   | firstName | lastName | dateOfBirth | address.email    | address.work                  | organization |
      | e53ef4cc-3e03-48e0-8088-fa95847f87bf | Jane      | Doe      | 25-10-1974  | jane@example.com | 123 Short St. Sydney NSW 2000 | snug         |
    When i create users
    Then i check if create user http status is 400
    And i check if no data was sent in the response